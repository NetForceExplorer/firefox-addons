# Firefox Web Extensions

[Home-Manger](https://github.com/nix-community/home-manager) allows to configure firefox profiles declaritively, including extensions. This repository aims to provide as many extensions as possible by crawling the web store and generating nix expressions.

This repository reuses a lot of work done by [rycee](https://github.com/rycee). Namely [mozilla-addons-to-nix](https://sr.ht/~rycee/mozilla-addons-to-nix/) and his repo structure on [NUR](https://github.com/nix-community/nur-combined/tree/master/repos/rycee/pkgs/firefox-addons). 

In addition to his work, I added some rudimentary wrapping:
- ./fetch_addons/fetch_addons: python script that crawls the webstore and filters out the individual extenstions.
- ./fetch_addons/generate_json.sh: takes the individual extensions and formats them into the json format that mozilla-addos-to-nix can take.
- ./fetch_addons/run_addons_to_nix.sh: this wrapper around mozilla-addons-to-nix uses the json files from generate_json.sh to generate nix expressions.
- ./fetch_addons/combine_derivation.sh: combines all nix expressions into one file that is loaded by default.nix
- ./fetch_addons/zotero/update_zotero.sh: generates the nix-expression for the zotero-connector and updates it for newer versions

# ToDo
- [x] Systemd service to update repo regularly
- [x] Fix Argument list to long error for parallel to compute faster
- [ ] Make scripts more readable and add documentation
