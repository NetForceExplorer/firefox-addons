#!/usr/bin/env bash


nix develop --command bash -c "python fetch_addons.py && bash generate_json.sh && bash run_addons_to_nix.sh && bash combine_derivation.sh"
