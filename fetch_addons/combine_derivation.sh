#!/usr/bin/env bash

read -a arr_addons -r -d'\n' <<< "$(find "${DERIVATION_FOLDER}" -name "*.nix")"

echo -e "{ buildFirefoxXpiAddon, fetchurl, lib, stdenv }:\n{" > "${COMBINED_PATH}"/combined.nix

for addon in "${arr_addons[@]}"; do
    sed -n '3,$p' "${addon}" | sed '$ d' >> "${COMBINED_PATH}"/combined.nix
done

"${ZOTERO_PATH}"/update_zotero.sh
sed -n '3,$p' "${ZOTERO_PATH}"/zotero.nix | sed '$ d'  >> "${COMBINED_PATH}"/combined.nix

echo "}" >> "${COMBINED_PATH}"/combined.nix

nixpkgs-fmt "${COMBINED_PATH}"/combined.nix "${COMBINED_PATH}"/flake.nix
