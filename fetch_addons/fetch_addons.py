#!/user/bin/env python
import requests
from bs4 import BeautifulSoup
import json
from progress.bar import Bar

def get_extensions():
    url = 'https://addons.mozilla.org/af/firefox/search/?type=extension&page='
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}
    extension_list = []
    with Bar('Fetch:', max=1200) as bar:
        for i in range(1,1200 + 1):
            response = requests.get(f"{url}{i}", headers=headers)
            if response.status_code == 200:
                #soup = BeautifulSoup(response.text, 'html.parser')
                extension_list.append(response.text)
                bar.next()
            else:
                print('Failed to fetch data')
                return None
    return extension_list


def save_to_json(data):
    with open('firefox_extensions.json', 'w') as f:
        json.dump(data, f, indent=4)

def read_list_from_json(fname):
    with open(fname, "r") as f:
        json_list= json.load(f)

    return json_list

def get_addon(j_ele, ext_path = "ext"):
    soup = BeautifulSoup(j_ele, "html.parser")

    script = soup.find("script")

    res = json.loads(script.text)

    for ext in res["search"]["results"]:

        pname = ext["url"].split("/")[-2]

        try:
            with open(f"{ext_path}/{pname}.json", "w") as g:
                g.write(json.dumps(ext, ensure_ascii=False))
        except:
            print(pname)

if __name__ == "__main__":
    extensions = get_extensions()
    if extensions:
        save_to_json(extensions)
        print("Extensions data saved to 'firefox_extensions.json'")

    json_list = read_list_from_json("firefox_extensions.json")
    
    with Bar('Filter:', max=len(json_list)) as bar:
        for ele in json_list:
            get_addon(ele)
            bar.next()




