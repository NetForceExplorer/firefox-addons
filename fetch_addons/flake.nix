{
  description = "Dev shell for repo";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nur.url = "github:nix-community/NUR";
  };
  outputs = { self, nixpkgs, flake-utils, nur }:

    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [ nur.overlay ];

          };

          buildInputs = with pkgs; [
            jq
            wget
            unzip
            python311Full
            python311Packages.progress
            python311Packages.requests
            python311Packages.beautifulsoup4
            coreutils-full
            findutils
            binutils
            parallel-full
            nixpkgs-fmt
            pkgs.nur.repos.rycee.mozilla-addons-to-nix
          ];
          nativeBuidInputs = [ ];
        in
        {
          devShells.default = pkgs.mkShell {
            inherit buildInputs nativeBuidInputs;
            shellHook = ''
              	      export EXT_FOLDER="$(realpath ./ext)";
              	      export MOZILLA_ADDONS_JSON="$(realpath ./input)";
              	      export MOZILLA_ADDONS_PATH="$(realpath ../mozilla-addons-to-nix)";
              	      export DERIVATION_FOLDER="$(realpath ./nix)";
              	      export COMBINED_PATH="$(realpath ../)";
              	      export ZOTERO_PATH="$(realpath ./zotero)";
              	      mkdir -p ''${EXT_FOLDER} ''${MOZILLA_ADDONS_JSON} ''${DERIVATION_FOLDER}
                            	    '';

          };
        });
}
