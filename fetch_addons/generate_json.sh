#!/usr/bin/env bash

#export EXT_FOLDER="$(realpath ./ext)"
#export MOZILLA_ADDONS_JSON="$(realpath ./input)"


# create a json with the pname as slug 
# that can be used by mozilla-addons-to-nix
function generate_json_slug() {
    local addon="$1"
    name=$(basename "${addon/.json//}")
    new_json="[\n{\"slug\": \"${name}\"}\n]"
    echo -e "${new_json}" > "${MOZILLA_ADDONS_JSON}"/"${name}".json
}
export -f generate_json_slug


# find all plugins and execute the slug json generation
find "${EXT_FOLDER}" -name "*.json" -print0 \
    | parallel --null -j"$(nproc)" generate_json_slug {} 

#function unified_addon_list() {
#    local addon="$1"
#    name=$(basename "${addon/.json//}")
#    echo -e "{\"slug\": \"${name}\"},"
#}
#export -f unified_addon_list
#
#echo -e "[\n" > ./addons.json
# find all plugins and execute the slug json generation
#find "${EXT_FOLDER}" -name "*.json" -print0 \
#    | parallel --null -j"$(nproc)" unified_addon_list {} >> ./addons.json
#
#echo -e "\n]" >> ./addons.json
#sed -i -z 's/\(.*\),/\1/' ./addons.json # remove last comma for valid json
