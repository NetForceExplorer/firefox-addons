#!/usr/bin/env bash
#export MOZILLA_ADDONS_JSON="$(realpath ./input)"
#export MOZILLA_ADDONS_PATH="$(realpath ../mozilla-addons-to-nix)"
#export DERIVATION_FOLDER="$(realpath ./nix)"

rm -f "${DERIVATION_FOLDER}"/failed.log
rm -f "${DERIVATION_FOLDER}"/version.log

function generate_nix_expression () {
    local addon="$1"
    # retry and wait if parsing failed
    for _ in $(seq 1 5); do 
	mozilla-addons-to-nix "${addon}" "${DERIVATION_FOLDER}"/"$(basename "${addon}")".nix >> "${DERIVATION_FOLDER}"/version.log 2> /dev/null && break  || sleep 1 && echo "${DERIVATION_FOLDER}/$(basename "${addon}").nix" >> "${DERIVATION_FOLDER}"/failed.log
    done
}
export -f generate_nix_expression
find "${MOZILLA_ADDONS_JSON}" -type f -print0 \
    | parallel --null -j2 generate_nix_expression {}

