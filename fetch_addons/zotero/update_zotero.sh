#!/usr/bin/env bash

URL="https://www.zotero.org/download/connector/dl?browser=firefox"
ZOTERO_TMP="/tmp/zotero-version-check"
mkdir -p ${ZOTERO_TMP} 

wget -O zotero.xpi "${URL}"
HASH="$(nix-hash --flat --base32 --type sha256 zotero.xpi)"


rm -rf ${ZOTERO_TMP}
unzip zotero.xpi -d ${ZOTERO_TMP} || exit 1

VERSION="$(jq ".version" ${ZOTERO_TMP}/manifest.json)"
PERMISSIONS=$(jq ".permissions" ${ZOTERO_TMP}/manifest.json | sed 's/,//g' | sed 's/\[//g' | sed 's/\]//g')
OPTIONAL_PERMISSIONS=$(jq ".optional_permissions" ${ZOTERO_TMP}/manifest.json | sed 's/,//g' | sed 's/\[//g' | sed 's/\]//g')

cat << EOF > "${ZOTERO_PATH}"/zotero.nix
{ buildFirefoxXpiAddon, fetchurl, lib, stdenv }:
  {
    zotero-connector =
      let version = ${VERSION};
      in buildFirefoxXpiAddon {
        pname = "zotero-connector";
        inherit version;
        addonId = "zotero@chnm.gmu.edu";
        url = "https://www.zotero.org/download/connector/dl?browser=firefox&version=\${version}";
        sha256 = "${HASH}";
        mozPermissions = [
    	${PERMISSIONS}
    	${OPTIONAL_PERMISSIONS}
        ];
        meta = with lib; {
          homepage =
            "https://www.zotero.org/";
          description = "Automatically connect to university proxy.";
    
          license = licenses.gpl3Plus;
          platforms = platforms.all;
        };
      };
  }
EOF

nixpkgs-fmt "${ZOTERO_PATH}"/zotero.nix

rm -rf zotero.xpi "${ZOTERO_PATH}"/${ZOTERO_TMP}
