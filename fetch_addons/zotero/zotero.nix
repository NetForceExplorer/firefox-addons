{ buildFirefoxXpiAddon, fetchurl, lib, stdenv }:
{
  zotero-connector =
    let version = "5.0.151";
    in buildFirefoxXpiAddon {
      pname = "zotero-connector";
      inherit version;
      addonId = "zotero@chnm.gmu.edu";
      url = "https://www.zotero.org/download/connector/dl?browser=firefox&version=${version}";
      sha256 = "0gda0p4ddirrc98mv5x6kx6kl57m0jhw76i3jc04n88r7fyzjk50";
      mozPermissions = [

        "http://*/*"
        "https://*/*"
        "tabs"
        "contextMenus"
        "cookies"
        "webRequest"
        "webRequestBlocking"
        "webNavigation"
        "storage"

        "management"
        "clipboardWrite"
      ];
      meta = with lib; {
        homepage =
          "https://www.zotero.org/";
        description = "Automatically connect to university proxy.";

        license = licenses.gpl3Plus;
        platforms = platforms.all;
      };
    };
}
